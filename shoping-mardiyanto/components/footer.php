<footer class="footer">

   <section class="grid">

      <div class="box">
         <h3>quick links</h3>
         <a href="home.php"> <i class="fas fa-angle-right"></i> home</a>
         <a href="about.php"> <i class="fas fa-angle-right"></i> about</a>
         <a href="shop.php"> <i class="fas fa-angle-right"></i> shop</a>
         <a href="contact.php"> <i class="fas fa-angle-right"></i> contact</a>
      </div>

      <div class="box">
         <h3>extra links</h3>
         <a href="user_login.php"> <i class="fas fa-angle-right"></i> login</a>
         <a href="user_register.php"> <i class="fas fa-angle-right"></i> register</a>
         <a href="cart.php"> <i class="fas fa-angle-right"></i> keranjang</a>
         <a href="orders.php"> <i class="fas fa-angle-right"></i> pesanan</a>
      </div>

      <div class="box">
         <h3>contact us</h3>
         <a href="tel:1234567890"><i class="fas fa-phone"></i>082172445877</a>
         <a href="tel:11122233333"><i class="fas fa-phone"></i>082284366149</a>
         <a href="mardiyanto.tkj2@gmail.com"><i class="fas fa-envelope"></i> mardiyanto.tkj2@gmail.com</a>
         <a href="https://goo.gl/maps/rCZHs6scR96pz2TUA"><i class="fas fa-map-marker-alt"></i> lukun,tebing tinggi timur. </a>
      </div>

      <div class="box">
         <h3>follow us</h3>
         <a href="https://www.facebook.com/yoo.t.1238"><i class="fab fa-facebook-f"></i>Mardiyanto</a>
         <a href="#"><i class="fab fa-twitter"></i>toyyo_</a>
         <a href="https://www.instagram.com/mardyto_/?hl=id"><i class="fab fa-instagram"></i>mrdynto_</a>
         <a href="#"><i class="fab fa-linkedin"></i>linkedin</a>
      </div>

   </section>

   <div class="credit">&copy; copyright @ <?= date('Y'); ?> by <span>Mardiyanto</span> | all rights reserved!</div>

</footer>